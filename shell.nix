let
  oxalica_overlay = import (builtins.fetchTarball
    "https://github.com/oxalica/rust-overlay/archive/master.tar.gz");
  nixpkgs = import <nixpkgs> { overlays = [ oxalica_overlay ]; };

in with nixpkgs;

stdenv.mkDerivation {
  name = "rust-env";
  buildInputs = [
    (rust-bin.stable.latest.default.override {
      extensions = [ "rust-src" ];
      targets = [ "wasm32-unknown-unknown" ];
    })

    # Possibly needed for some crates like reqwest
    pkg-config
    openssl
    trunk
  ];

  # Set Environment Variables
  RUST_BACKTRACE = 1;

}


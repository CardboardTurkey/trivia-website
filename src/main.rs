use yew::prelude::*;

// #[derive(Clone, PartialEq)]
// struct Video {
//     id: usize,
//     title: String,
//     speaker: String,
//     url: String,
// }

#[derive(Clone, PartialEq)]
struct Question {
    id: usize,
    text: String,
}

// #[derive(Properties, PartialEq)]
// struct VideosListProps {
//     videos: Vec<Video>,
//     on_click: Callback<Video>,
// }

// #[derive(Properties, PartialEq)]
// struct VideosDetailsProps {
//     video: Video,
// }
#[derive(Properties, PartialEq)]
struct QuestionProps {
    question: Question,
}

// #[function_component(VideoDetails)]
// fn video_details(VideosDetailsProps { video }: &VideosDetailsProps) -> Html {
//     html! {
//         <div>
//             <h3>{ video.title.clone() }</h3>
//             <img src="https://via.placeholder.com/640x360.png?text=Video+Player+Placeholder" alt="video thumbnail" />
//         </div>
//     }
// }
#[function_component(QuestionComponent)]
fn question_component(QuestionProps { question }: &QuestionProps) -> Html {
    html! {
        <div>
            <p key={question.id}>{question.text.clone()}</p>
        </div>
    }
}

// looks like function name can be whatever
// #[function_component(VideosList)]
// fn videos_list(VideosListProps { videos, on_click }: &VideosListProps) -> Html {
//     videos
//         .iter()
//         .map(|video| {
//             let on_video_select = {
//                 let on_click = on_click.clone();
//                 let video = video.clone();
//                 Callback::from(move |_| on_click.emit(video.clone()))
//             };
//             html! {
//                 <p key={video.id} onclick={on_video_select}>{format!("{}: {}", video.speaker, video.title)}</p>
//             }
//         })
//         .collect()
// }

#[function_component(App)]
fn app() -> Html {
    // let videos = vec![
    //     Video {
    //         id: 1,
    //         title: "Building and breaking things".to_string(),
    //         speaker: "John Doe".to_string(),
    //         url: "https://youtu.be/PsaFVLr8t4E".to_string(),
    //     },
    //     Video {
    //         id: 2,
    //         title: "The development process".to_string(),
    //         speaker: "Jane Smith".to_string(),
    //         url: "https://youtu.be/PsaFVLr8t4E".to_string(),
    //     },
    //     Video {
    //         id: 3,
    //         title: "The Web 7.0".to_string(),
    //         speaker: "Matt Miller".to_string(),
    //         url: "https://youtu.be/PsaFVLr8t4E".to_string(),
    //     },
    //     Video {
    //         id: 4,
    //         title: "Mouseless development".to_string(),
    //         speaker: "Tom Jerry".to_string(),
    //         url: "https://youtu.be/PsaFVLr8t4E".to_string(),
    //     },
    // ];
    let questions = vec![
        Question {
            id: 1,
            text: "Question 1?".to_owned(),
        },
        Question {
            id: 2,
            text: "Question 2?".to_owned(),
        },
        Question {
            id: 3,
            text: "Question 3?".to_owned(),
        },
        Question {
            id: 4,
            text: "Question 4?".to_owned(),
        },
    ];

    let mut q_num = 0;
    let mut question_iterator = questions.into_iter().cycle();
    let first_q = question_iterator.next();

    // let selected_video = use_state(|| None);
    let current_question = use_state(|| first_q);

    // let on_video_select = {
    //     let selected_video = selected_video.clone();
    //     Callback::from(move |video: Video| selected_video.set(Some(video)))
    // };
    let next_question = {
        let this_q = question_iterator.next();
        let current_question = current_question.clone();
        Callback::from(move |_| current_question.set(this_q.clone()))
    };

    let question_html = current_question.as_ref().map(|question| {
        html! {
            <QuestionComponent question={question.to_owned()} />
        }
    });

    // let details = selected_video.as_ref().map(|video| {
    //     html! {
    //         <VideoDetails video={video.clone()} />
    //     }
    // });

    html! {
        <>
            // <h1>{ "RustConf Explorer" }</h1>
            // <div>
            //     <h3>{"Videos to watch"}</h3>
            //     <VideosList videos={videos} on_click={on_video_select.clone()} />
            // </div>
            { for question_html }
            <div>
                <button onclick={next_question}>{"Next"}</button>
            </div>
            // { for details }
        </>
    }
}

fn main() {
    wasm_logger::init(wasm_logger::Config::default());
    log::trace!("Initializing yew...");
    yew::Renderer::<App>::new().render();
}
